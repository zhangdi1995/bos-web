import Vue from 'vue'
import Router from 'vue-router'
import homepage from '../page/homepage'
import films from '../page/films'
import education from '../page/education'
import about from '../page/about'
import contact from '../page/contact'
import blackandwhite from '../page/blackandwhite'
import bubbling2 from '../page/bubbling2'
import crystal2 from '../page/crystal2'
import elementsburning from '../page/elementsburning'
import precipitation2 from '../page/precipitation2'
import precipitation3 from '../page/precipitation3'
import metals from '../page/metals'


Vue.use(Router)

const routers= [
    {
      path: '/',
      component:(resolve) =>{ require(['@/page/homepage'],resolve)
      },
      meta: {
        title: '重现化学'
      }
    },
    {
      path: '/films',
      component:(resolve) =>{ require(['@/page/films'],resolve)
      },
      meta: {
        title: '系列影片-重现化学'
      }
    },
    {
      path: '/education',
      component:(resolve) =>{ require(['@/page/education'],resolve)
      },
      meta: {
        title: '化学教学-重现化学'
      }

    },
    {
      path: '/about',
      component:(resolve) =>{ require(['@/page/about'],resolve)
      },
      meta: {
        title: '关于我们-重现化学'
      }
    },
    {
      path: '/contact',
      component:(resolve) =>{ require(['@/page/contact'],resolve)
      },
      meta: {
        title: '联系我们-重现化学'
      }
    },
    {
    path: '/blackandwhite',
    component:(resolve) =>{ require(['@/page/blackandwhite'],resolve)
    },
    meta: {
      title: '系列影片-重现化学'
    }
  },
    {
    path: '/bubbling2',
    component:(resolve) =>{ require(['@/page/bubbling2'],resolve)
    },
    meta: {
      title: '气体2-重现化学'
    }
  },
    {
    path: '/crystal2',
    component:(resolve) =>{ require(['@/page/crystal2'],resolve)
    },
    meta: {
      title: '结晶2-重现化学'
    }
  },
    {
    path: '/elementsburning',
    component:(resolve) =>{ require(['@/page/elementsburning'],resolve)
    },
    meta: {
      title: '元素的燃烧-重现化学'
    }
  },
    {
    path: '/precipitation2',
    component:(resolve) =>{ require(['@/page/precipitation2'],resolve)
    },
    meta: {
      title: '沉淀2-重现化学'
    }
  },
    {
    path: '/precipitation3',
    component:(resolve) =>{ require(['@/page/precipitation3'],resolve)
    },
    meta: {
      title: '沉淀3-重现化学'
    }
  },
    {
    path: '/metals',
    component:(resolve) =>{ require(['@/page/metals'],resolve)
    },
    meta: {
      title: '消失的金属-重现化学'
    }
  },
  ]
// router.afterEach((to, from, next) => {
//   window.scrollTo(0, 0);
// });
export default routers
